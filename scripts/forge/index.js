const filenamify = require('filenamify'); // eslint-disable-line import/no-extraneous-dependencies
const slugify = require('@sindresorhus/slugify'); // eslint-disable-line import/no-extraneous-dependencies
const fse = require('fs-extra'); // eslint-disable-line import/no-extraneous-dependencies
const path = require('path'); // eslint-disable-line import/no-extraneous-dependencies
const replace = require('replace-in-file'); // eslint-disable-line import/no-extraneous-dependencies
const yargs = require('yargs').argv; // eslint-disable-line import/no-extraneous-dependencies

const { forgeDirectories } = require('../../configuration');

const forgeConfig = {
  component: {
    getValue: (value) => {
      if (value[0] !== value[0].toUpperCase()) {
        throw new Error('The Component name is not properly cased. Please use UpperCamelCase, referred to as PascalCase');
      }
      return value;
    },
    getCopyPath: fileName => ({
      source: path.join(forgeDirectories.component, 'Boilerplate'),
      destination: path.join(forgeDirectories.component, fileName),
    }),
  },
  utility: {
    getValue: value => filenamify(slugify(value, { replacement: '-' })),
    getCopyPath: fileName => ({
      source: path.join(forgeDirectories.utility, '_Boilerplate.js'),
      destination: path.join(forgeDirectories.utility, `${fileName}.js`),
    }),
  },
};

const forgeKeys = Object.keys(forgeConfig);

const isDirectory = checkPath => fse.statSync(checkPath).isDirectory();

const getFiles = (directory, filelist) => {
  const files = fse.readdirSync(directory);
  let fileList = filelist || [];

  files.forEach((file) => {
    const absolutePath = path.join(directory, file);
    if (isDirectory(absolutePath)) {
      fileList = getFiles(absolutePath, fileList);
    } else {
      fileList.push(absolutePath);
    }
  });

  return fileList;
};

const forge = (key, value) => {
  const paths = forgeConfig[key].getCopyPath(value);

  if (fse.existsSync(paths.destination)) {
    return console.warn(`WARNING: ${paths.destination} already exists. Moving on.`); // eslint-disable-line no-console
  }

  fse.copySync(paths.source, paths.destination);

  const destinationIsDirectory = fse.lstatSync(paths.destination).isDirectory();
  const files = (destinationIsDirectory) ? getFiles(paths.destination) : [paths.destination];

  replace.sync({
    files,
    from: [/Boilerplate/g, /boilerplate/g],
    to: [value, `${value.charAt(0).toLowerCase()}${value.slice(1)}`],
  });

  return false;
};

module.exports = (() => {
  const validKeys = forgeKeys
    .filter(key => Object.prototype.hasOwnProperty.call(yargs, key));

  if (validKeys.length === 0) {
    const error = new Error(`The argument is not supported, please choose one of: [${forgeKeys.join(', ')}]`);
    throw error;
  }

  return validKeys.forEach((key) => {
    forge(key, forgeConfig[key].getValue(yargs[key]));
  });
})();
