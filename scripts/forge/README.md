# Notes
- why `forge`? Well yarn already took `create`, so we needed something else and it was the coolest sounding synonym

# TO-DO
- add `promotion` flag or similar to track forged files and allow for easy cleanup later
  - might not be worth it, especially if files are created outside this `forge` utility script
-