# Setup

## Repo

1. Initialize a gatsby project via `gatsby new [REPO_NAME]`
2. Remove extraneous `package-lock.json` in favor of `yarn.lock`
3. Initialize the project as a git repo via `git init`
4. Setup a repo in bitbucket
5. Setup remote `origin` using the bitbucket git URL `git remote add origin git@bitbucket.org:psullivan6/[REPO_NAME].git`
6. Setup remote `dropbox` via the [README](https://www.dropbox.com/s/havu4dzrzj86wyq/README.md?dl=0)
7. Commit and push to `origin` and `dropbox`

## Customization

1. Add `styled-components` package
2. Add `prop-types` package
3. Add directory alias preferences to the `gatsby-node.js` file
4. Add `eslint` package with AirBnB standard `yarn run eslint --init`
  1. Install Peer Dependencies `yarn add eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react -D`
5. Configure ESLint to match preferences
  1. Added `"import/no-unresolved" : "off"` rule so as to avoid alias directory issue with Gatsby's lack of exposed webpack config
