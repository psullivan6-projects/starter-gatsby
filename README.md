# Usage

To use this "template" repo, clone it and replace the git information

1. `git clone --depth=1 git@bitbucket.org:psullivan6/starter-gatsby.git [REPO_NAME]`
2. `cd [REPO_NAME]`
3. `rm -rf .git/`
4. `git init`
5. `git remote add origin git@bitbucket.org:psullivan6/[REPO_NAME].git`
6. Update the `package.json` with appropriate information
6. Update the `now.json` with appropriate information
7. Update this `README.md` with appropriate information
8. `git add .`
9. `git commit -m "initial setup"`
10. `git push origin master`
11. DONE
