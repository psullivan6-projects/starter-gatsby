const path = require('path');

const Components = path.resolve(__dirname, './src/components');
const Utilities = path.resolve(__dirname, './src/utilities');

exports.aliases = {
  Components,
  Data: path.resolve(__dirname, './src/data'),
  Images: path.resolve(__dirname, './src/images'),
  Pages: path.resolve(__dirname, './src/pages'),
  Utilities,
};

// Easier than handling lowercasing and de-pluralizing of the alias values
// This is used in the `scripts/forge` utility script
exports.forgeDirectories = {
  component: Components,
  utility: Utilities,
};
