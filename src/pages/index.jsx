import React from 'react';
import { Link } from 'gatsby';

// Components
import Boilerplate from 'Components/Boilerplate';
import Layout from 'Components/Layout';


const IndexPage = () => (
  <Layout>
    <Boilerplate />
    <Link to="/page-2/">Go to page 2</Link>
  </Layout>
);

export default IndexPage;
