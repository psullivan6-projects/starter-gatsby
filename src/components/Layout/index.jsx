import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { StaticQuery, graphql } from 'gatsby';

// Styles
import 'normalize.css';
import GlobalStyle from './styles';


const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: 'Custom Gatsby Starter repo' },
            { name: 'keywords', content: 'custom, starter, gatsby, repo, boilerplate' },
          ]}
        >
          <html lang="en" />
          <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i|IBM+Plex+Sans:400,700" rel="stylesheet" />
        </Helmet>
        <GlobalStyle />
        { children }
      </>
    )}
  />
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
