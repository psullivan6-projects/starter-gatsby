import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 2rem;
    font-family: 'IBM Plex Sans', sans-serif;
    /* font-family: 'IBM Plex Mono', monospace; */
  }
`;

export default GlobalStyle;
