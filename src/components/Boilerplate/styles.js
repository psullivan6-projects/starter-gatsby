import styled from 'styled-components';

export const Headline = styled.h1`
  margin: 0;
  font-size: 1rem;
  line-height: 1;
  color: #39c;
`;

export default {
  Headline,
};
