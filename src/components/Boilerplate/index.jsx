import React from 'react';
import PropTypes from 'prop-types';

// Styles
import { Headline } from './styles';


const Boilerplate = ({ TEST }) => (
  <section>
    <Headline>Boilerplate</Headline>
    <h2>{ TEST }</h2>
  </section>
);

Boilerplate.propTypes = {
  TEST: PropTypes.string,
};

Boilerplate.defaultProps = {
  TEST: 'TEST prop',
};

export default Boilerplate;
